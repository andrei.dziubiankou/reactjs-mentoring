import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import {
  loadFilms,
  loadFilm,
  searchChange,
  sortByChange,
  searchByChange,
  sortByChangeAndFetch,
  fetchFilm,
  setDefaultState,
} from './index';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Actions', () => {
  afterEach(() => {
    fetchMock.restore();
  });

  it('loadFilms should be return correct action', () => {
    const expectedAction = {
      type: 'LOAD_FILMS',
      films: [1, 2, 3],
    };
    const actualAction = loadFilms([1, 2, 3]);
    expect(actualAction).toEqual(expectedAction);
  });

  it('loadFilm should be return correct action', () => {
    const expectedAction = {
      type: 'LOAD_FILM',
      film: { title: 'test' },
    };
    const actualAction = loadFilm({ title: 'test' });
    expect(actualAction).toEqual(expectedAction);
  });

  it('searchChange should be return correct action', () => {
    const expectedAction = {
      type: 'CHANGE_SEARCH',
      search: 'test search',
    };
    const actualAction = searchChange('test search');
    expect(actualAction).toEqual(expectedAction);
  });

  it('sortByChange should be return correct action', () => {
    const expectedAction = {
      type: 'CHANGE_SORTBY',
      sortBy: 'sort by change',
    };
    const actualAction = sortByChange('sort by change');
    expect(actualAction).toEqual(expectedAction);
  });

  it('searchByChange should be return correct action', () => {
    const expectedAction = {
      type: 'CHANGE_SEARCHBY',
      searchBy: 'search by change',
    };
    const actualAction = searchByChange('search by change');
    expect(actualAction).toEqual(expectedAction);
  });

  it('setDefaultState should be return correct action', () => {
    const expectedAction = {
      type: 'SET_DEFAULT',
    };

    const actualAction = setDefaultState();
    expect(actualAction).toEqual(expectedAction);
  });

  it('sortByChangeAndFetch should be return correct actions', () => {
    const searchParams = {
      sortBy: 'value',
      search: 'test',
    };
    const expectedActions = [{
      sortBy: 'value',
      type: 'CHANGE_SORTBY',
    }, {
      type: 'LOAD_FILMS',
      films: [1, 2, 3],
    }];
    fetchMock.getOnce('http://react-cdp-api.herokuapp.com/movies?sortBy=value&search=test', {
      body: { data: [1, 2, 3] },
      headers: { 'content-type': 'application/json' },
    });

    const store = mockStore({ films: [] });

    return store.dispatch(sortByChangeAndFetch(searchParams)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('fetchFilm should be return correct actions', () => {
    const expectedActions = [{
      film: { title: 'test', genres: ['123'], id: 23 },
      type: 'LOAD_FILM',
    }, {
      films: [1, 2, 3],
      type: 'LOAD_FILMS',
    }];
    fetchMock
      .get('http://react-cdp-api.herokuapp.com/movies/23', {
        body: { title: 'test', genres: ['123'], id: 23 },
        headers: { 'content-type': 'application/json' },
      })
      .get('http://react-cdp-api.herokuapp.com/movies?searchBy=genres&search=123', {
        body: { data: [1, 2, 3] },
        headers: { 'content-type': 'application/json' },
      });

    const store = mockStore({ films: [] });

    return store.dispatch(fetchFilm(23)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
