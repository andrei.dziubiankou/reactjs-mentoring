export function loadFilms(films) {
  return {
    type: 'LOAD_FILMS',
    films,
  };
}

export function loadFilm(film) {
  return {
    type: 'LOAD_FILM',
    film,
  };
}

export function searchChange(search) {
  return {
    type: 'CHANGE_SEARCH',
    search,
  };
}

export function sortByChange(sortBy) {
  return {
    type: 'CHANGE_SORTBY',
    sortBy,
  };
}

export function searchByChange(searchBy) {
  return {
    type: 'CHANGE_SEARCHBY',
    searchBy,
  };
}

export function setDefaultState() {
  return {
    type: 'SET_DEFAULT',
  };
}

export function fetchFilms(searchParams) {
  return (dispatch) => {
    const query = `?${Object.keys(searchParams)
      .map(prop => (searchParams[prop] ? `${prop}=${searchParams[prop]}` : null))
      .filter(elem => elem).join('&')}`;
    if (searchParams.search) {
      return fetch(`http://react-cdp-api.herokuapp.com/movies${query}`)
        .then(response => response.json())
        .then(json => dispatch(loadFilms(json.data)));
    }
    dispatch(setDefaultState());

    return null;
  };
}

export function sortByChangeAndFetch(searchParams) {
  return (dispatch) => {
    dispatch(sortByChange(searchParams.sortBy));
    return dispatch(fetchFilms(searchParams));
  };
}

export function fetchFilm(id) {
  return dispatch => fetch(`http://react-cdp-api.herokuapp.com/movies/${id}`)
    .then(response => response.json())
    .then((json) => {
      if (json.id) {
        const searchParams = {
          searchBy: 'genres',
          search: json.genres[0],
        };
        dispatch(loadFilm(json));
        // if don't return, then ssr will rendered only
        // info about film, but not list of similar films
        return dispatch(fetchFilms(searchParams));
      }
      dispatch(setDefaultState());
      // this return need for consistent-return eslint rule
      return dispatch(loadFilm(null));
    });
}
