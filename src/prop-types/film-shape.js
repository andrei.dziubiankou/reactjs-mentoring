export type Film = {
  release_date: string,
  title: string,
  genres: string[],
  overview: string,
  vote_average: number,
  runtime: number,
  poster_path: string,
}
