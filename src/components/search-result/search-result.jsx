// @flow
import React from 'react';
import injectSheet from 'react-jss';
import SortButton from '../sort-button';

const styles = {
  result: {
    display: 'flex',
    justifyContent: 'space-between',
  },
};

type Props = {
  genre?: string,
  filmsFounded?: number,
  sorters?: string[],
  sortBy?: string,
  sortByOnChange?: Function,
  classes: {
    result: {}
  }
};

const SearchResult = ({
  classes, genre, filmsFounded, sorters, sortBy, sortByOnChange,
}: Props) => {
  let resultMessage;
  let sortersList;
  resultMessage = (
    <div>
      {`${filmsFounded || 0} movies found`}
    </div>
  );
  if (genre) {
    resultMessage = (
      <div>
        {`Films by ${genre} genre`}
      </div>
    );
  }
  if (sorters && sorters.length) {
    sortersList = (
      <div>
        {'Sort by '}
        {sorters.map(type => (
          <SortButton type={type} key={type} checked={sortBy === type} onChange={sortByOnChange} />
        ))}
      </div>
    );
  }
  return (
    <div className={classes.result}>
      {resultMessage}
      {sortersList}
    </div>
  );
};

SearchResult.defaultProps = {
  genre: '',
  filmsFounded: 0,
  sorters: [],
  sortBy: '',
  sortByOnChange: () => {},
};

export default injectSheet(styles)(SearchResult);
