import React from 'react';
import { shallow } from 'enzyme';
import SearchResult from './search-result';

describe('SearchResult component', () => {
  it('should be render correctly', () => {
    const component = shallow(<SearchResult />);
    expect(component).toMatchSnapshot();
  });

  it('should be render correctly with sorters and films founded', () => {
    const component = shallow(
      <SearchResult sorters={['rating', 'release date']} filmsFounded={4} />,
    );
    expect(component).toMatchSnapshot();
  });

  it('should be render correctly with genre displayed without sorters', () => {
    const component = shallow(<SearchResult genre="Horror" />);
    expect(component).toMatchSnapshot();
  });
});
