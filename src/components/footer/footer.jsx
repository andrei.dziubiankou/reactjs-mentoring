// @flow
import React from 'react';
import injectSheet from 'react-jss';

const styles = {
  footer: {
    position: 'absolute',
    bottom: 0,
  },
};

type Props = {
  classes: {
    footer: {}
  }
};

const Footer = ({ classes }: Props) => (
  <footer className={classes.footer}>Developed by Andrei Dziubiankou</footer>
);

export default injectSheet(styles)(Footer);
