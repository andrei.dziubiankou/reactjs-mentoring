// @flow
import React from 'react';

type Props = {
  location: {
    pathname: string
  }
};

const NoMatch = ({ location }: Props) => (
  <h3>
    No match for
    {' '}
    <code>{location.pathname}</code>
  </h3>
);

export default NoMatch;
