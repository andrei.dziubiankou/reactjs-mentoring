import React from 'react';
import { shallow } from 'enzyme';
import Header from './header';

describe('Header component', () => {
  it('should be render correctly', () => {
    const component = shallow(<Header />);
    expect(component).toMatchSnapshot();
  });

  it('should be render correctly with Search button', () => {
    const component = shallow(<Header isSearchPresented />);
    expect(component).toMatchSnapshot();
  });
});
