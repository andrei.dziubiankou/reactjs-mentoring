// @flow
import React from 'react';
import { Link } from 'react-router-dom';
import injectSheet from 'react-jss';

const styles = {
  header: {
    height: '60px',
    display: 'flex',
    justifyContent: 'space-between',
  },
  logo: {
    fontSize: '1.5rem',
    color: 'red',
    textShadow: '1px 1px 1px black',
  },
};

type Props = {
  isSearchPresented?: boolean,
  classes: {
    logo: {},
    header: {}
  }
};

const Header = ({ classes, isSearchPresented }: Props) => (
  <header className={classes.header}>
    <div className={classes.logo}>netflixroutlette</div>
    {isSearchPresented
      ? <Link to="/"><button type="button">Search</button></Link>
      : ''}
  </header>
);

Header.defaultProps = {
  isSearchPresented: false,
};

export default injectSheet(styles)(Header);
