import React from 'react';
import { shallow } from 'enzyme';
import SortButton from './sort-button';

describe('SortButton component', () => {
  it('should be render correctly', () => {
    const component = shallow(<SortButton type="rating" onChange={() => {}} checked={false} />);
    expect(component).toMatchSnapshot();
  });

  it('should be call onChange', () => {
    const onChange = jest.fn();
    const component = shallow(<SortButton type="rating" onChange={onChange} checked={false} />);
    component.find('input[type="radio"]').simulate('change');
    expect(onChange).toHaveBeenCalled();
  });
});
