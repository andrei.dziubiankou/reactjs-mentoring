// @flow
import React from 'react';

type Props = {
  type: string,
  checked: boolean,
  onChange: Function
};

const SortButton = ({ type, checked, onChange }: Props) => {
  const id = `sortBy${type}`;
  return (
    <label htmlFor={id}>
      <input type="radio" name="sortType" value={type} id={id} checked={checked} onChange={onChange} />
      {type.replace('_', ' ')}
    </label>
  );
};

export default SortButton;
