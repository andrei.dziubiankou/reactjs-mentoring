// @flow
import React from 'react';
import injectSheet from 'react-jss';
import { List } from 'immutable';
import { Film } from '../../prop-types/film-shape';
import FilmPreview from '../film-preview';

const styles = {
  filmsList: {
    display: 'flex',
    flexWrap: 'wrap',
  },
};

type Props = {
  films: List<Film>,
  classes: {
    filmsList: {}
  }
};

const FilmsList = ({ classes, films }: Props) => {
  let result = <h1>No films found</h1>;
  if (films.size) {
    result = films.map(film => <FilmPreview film={film} key={`${film.releaseDate + film.genre + film.title}`} />);
  }
  return (
    <div className={classes.filmsList}>
      {result}
    </div>
  );
};

export default injectSheet(styles)(FilmsList);
