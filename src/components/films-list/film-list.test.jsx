import React from 'react';
import { shallow } from 'enzyme';
import FilmList from './film-list';
import films from '../../data/films';

describe('FilmList component', () => {
  it('should be render correctly', () => {
    const component = shallow(<FilmList films={films} />);
    expect(component).toMatchSnapshot();
  });

  it('should be render correctly with empty array correctly', () => {
    const component = shallow(<FilmList films={[]} />);
    expect(component).toMatchSnapshot();
  });
});
