// @flow
import React from 'react';
import injectSheet from 'react-jss';
import { Film } from '../../prop-types/film-shape';
import Header from '../header';

const styles = {
  head: {
    backgroundColor: 'grey',
    padding: '20px 20px',
  },
  filmFullInfo: {
    paddingLeft: '300px',
    minHeight: '300px',
    position: 'relative',
  },
  filmFullInfoImg: {
    width: '280px',
    height: '280px',
    position: 'absolute',
    left: '10px',
    top: '10px',
  },
};

type Props = {
  film?: Film,
  classes: {
    head: {},
    filmFullInfo: {},
    filmFullInfoImg: {}
  }
};

const FilmFullInfo = ({ classes, film }: Props) => (film ? (
  <div className={classes.head}>
    <Header isSearchPresented />
    <div className={classes.filmFullInfo}>
      <img src={film.poster_path} className={classes.filmFullInfoImg} alt="Poster" />
      <div>{film.release_date}</div>
      <div>{film.title}</div>
      <div>{film.genres ? film.genres.join(', ') : ''}</div>
      <div>{film.overview}</div>
      <div>{film.vote_average ? `Rating ${film.vote_average}` : ''}</div>
      <div>{film.runtime ? `Duration ${film.runtime} min` : ''}</div>
    </div>
  </div>
) : null);

FilmFullInfo.defaultProps = {
  film: {},
};

export default injectSheet(styles)(FilmFullInfo);
