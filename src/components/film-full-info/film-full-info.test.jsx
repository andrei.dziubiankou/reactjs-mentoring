import React from 'react';
import { shallow } from 'enzyme';
import FilmFullInfo from './full-film-info';

describe('FilmFullInfo component', () => {
  it('should be render correctly', () => {
    const film = {
      poster_path: 'https://pp.userapi.com/c851036/v851036500/c9fad/DuCvMaUfXKw.jpg',
      title: 'Name3',
      release_date: '1991',
      genres: ['horror'],
      overview: 'description',
      vote_average: 2,
      runtime: 153,
    };
    const component = shallow(<FilmFullInfo film={film} />);
    expect(component).toMatchSnapshot();
  });
});
