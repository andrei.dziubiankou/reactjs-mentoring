// @flow
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { List } from 'immutable';
import { Film } from '../../prop-types/film-shape';
import Search from '../search';
import SearchResult from '../search-result';
import FilmsList from '../films-list';
import {
  fetchFilms, searchChange, sortByChangeAndFetch, searchByChange, setDefaultState,
} from '../../actions';

const SORTERS = ['vote_average', 'release_date'];

type Props = {
  films: List<Film>,
  searchParams: {
    sortBy: string,
    searchBy: string,
    search: string,
    sortOrder: string,
  },
  match: {
    params: {
      search: string,
    },
  },
  history: {
    push: Function,
  },
  fetchFilmsConnect: Function,
  searchChangeConnect: Function,
  sortByChangeAndFetchConnect: Function,
  searchByChangeConnect: Function,
  setDefaultStateConnect: Function,
};

export class HomePage extends PureComponent<Props> {
  componentDidMount() {
    const {
      setDefaultStateConnect,
      searchChangeConnect,
      fetchFilmsConnect,
      match: { params: { search } },
      searchParams,
    } = this.props;
    if (search) {
      searchChangeConnect(search);
      fetchFilmsConnect({ ...searchParams, search });
    } else {
      setDefaultStateConnect();
    }
  }

  searchInputHandler = ({ target: { value } }: SyntheticInputEvent<>) => {
    const { searchChangeConnect } = this.props;
    searchChangeConnect(value);
  };

  searchSubmitHandler = () => {
    const { fetchFilmsConnect, searchParams, history } = this.props;
    history.push(`/search/${searchParams.search}`);
    fetchFilmsConnect(searchParams);
  };

  sortByHandler = ({ target: { value } }: SyntheticInputEvent<>) => {
    const { sortByChangeAndFetchConnect, searchParams } = this.props;
    sortByChangeAndFetchConnect({ ...searchParams, sortBy: value });
  };

  searchByHandler = ({ target: { value } }: SyntheticInputEvent<>) => {
    const { searchByChangeConnect } = this.props;
    searchByChangeConnect(value);
  };

  render() {
    const { films, searchParams: { searchBy, sortBy, search } } = this.props;
    return (
      <div id="homepage">
        <Search
          searchBy={searchBy}
          searchInputOnChange={this.searchInputHandler}
          searchOnSubmit={this.searchSubmitHandler}
          searchByOnChange={this.searchByHandler}
          search={search}
        />
        <SearchResult
          sorters={SORTERS}
          filmsFounded={films.size}
          sortByOnChange={this.sortByHandler}
          sortBy={sortBy}
        />
        <FilmsList films={films} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  films: state.films,
  searchParams: state.searchParams,
});

const mapDispatchToProps = {
  fetchFilmsConnect: fetchFilms,
  searchChangeConnect: searchChange,
  sortByChangeAndFetchConnect: sortByChangeAndFetch,
  searchByChangeConnect: searchByChange,
  setDefaultStateConnect: setDefaultState,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage);
