import React from 'react';
import { shallow } from 'enzyme';
import { HomePage } from './home-page';
import films from '../../data/films';

const searchParams = {
  sortBy: 'vote_average',
  searchBy: 'title',
  search: '',
  sortOrder: 'desc',
};

describe('HomePagecomponent', () => {
  it('should be render correctly', () => {
    const component = shallow(<HomePage
      films={films}
      searchParams={searchParams}
      setDefaultStateConnect={() => {}}
      match={{ params: { search: '' } }}
      history={{ push: () => {} }}
    />);
    expect(component).toMatchSnapshot();
  });

  it('should change sortBy correctly', () => {
    const sortByMock = jest.fn();
    const component = shallow(
      <HomePage
        films={films}
        sortByChangeAndFetchConnect={sortByMock}
        searchParams={searchParams}
        setDefaultStateConnect={() => {}}
        match={{ params: { search: '' } }}
        history={{ push: () => {} }}
      />,
    );
    component.instance().sortByHandler({ target: { value: 'testSearch' } });
    expect(sortByMock).toHaveBeenCalled();
  });

  it('should change searchBy correctly', () => {
    const searchByMock = jest.fn();
    const component = shallow(
      <HomePage
        films={films}
        searchByChangeConnect={searchByMock}
        searchParams={searchParams}
        setDefaultStateConnect={() => {}}
        match={{ params: { search: '' } }}
        history={{ push: () => {} }}
      />,
    );
    component.instance().searchByHandler({ target: { value: 'testSearch' } });
    expect(searchByMock).toHaveBeenCalled();
  });

  it('should change search correctly', () => {
    const searchMock = jest.fn();
    const component = shallow(
      <HomePage
        films={films}
        searchChangeConnect={searchMock}
        searchParams={searchParams}
        setDefaultStateConnect={() => {}}
        match={{ params: { search: '' } }}
        history={{ push: () => {} }}
      />,
    );
    component.instance().searchInputHandler({ target: { value: 'testSearch' } });
    expect(searchMock).toHaveBeenCalled();
  });
});
