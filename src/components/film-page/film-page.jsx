// @flow
import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { List } from 'immutable';
import { Film } from '../../prop-types/film-shape';
import FilmFullInfo from '../film-full-info';
import SearchResult from '../search-result';
import FilmsList from '../films-list';
import { fetchFilm } from '../../actions';

type Props = {
  fetchFilmConnect: (id: string) => void,
  film?: Film,
  films?: List<Film>,
  match: {
    params:{
      id: string,
    },
  },
};

export class FilmPage extends PureComponent<Props> {
  static defaultProps = {
    film: {},
    films: List([]),
  };

  componentDidMount() {
    const { match: { params: { id } } } = this.props;
    this.fetchFilm(id);
  }

  componentDidUpdate({ match: { params: { id: prevId } } }: Props) {
    const { match: { params: { id: newId } } } = this.props;
    if (prevId !== newId) {
      this.fetchFilm(newId);
    }
  }

  fetchFilm(id: string) {
    const { fetchFilmConnect } = this.props;
    fetchFilmConnect(id);
    window.scrollTo(0, 0);
  }

  render() {
    const { film, films } = this.props;
    if (!film) {
      return (
        <h3>
Film not found
          <Link to="/"><button type="button">Go home</button></Link>
        </h3>
      );
    }
    return (
      <Fragment>
        <FilmFullInfo film={film} />
        <SearchResult genre={(film && film.genres) ? film.genres[0] : ''} />
        <FilmsList films={films} />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  film: state.selectedFilm,
  films: state.films,
});

const mapDispatchToProps = { fetchFilmConnect: fetchFilm };

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(FilmPage));
