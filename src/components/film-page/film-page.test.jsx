import React from 'react';
import { shallow } from 'enzyme';
import { FilmPage } from './film-page';
import films from '../../data/films';

describe('FilmPage component', () => {
  it('should be render correctly', () => {
    const film = {
      poster_path: 'https://pp.userapi.com/c851036/v851036500/c9fad/DuCvMaUfXKw.jpg',
      title: 'Name3',
      release_date: '1991',
      genres: ['horror'],
      overview: 'description',
      vote_average: 2,
      runtime: 153,
    };

    const component = shallow(<FilmPage
      film={film}
      films={films}
      match={{ params: { id: '23' } }}
      fetchFilmConnect={() => {}}
    />);
    expect(component).toMatchSnapshot();
  });
});
