import React from 'react';
import { shallow } from 'enzyme';
import ErrorBoundary from './error-boundary';

describe('ErrorBoundary component', () => {
  it('should be render correctly', () => {
    const component = shallow(<ErrorBoundary />);
    expect(component).toMatchSnapshot();
  });
});
