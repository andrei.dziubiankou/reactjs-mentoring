import React from 'react';
import { shallow, mount } from 'enzyme';
import Search from './search';

describe('Search component', () => {
  it('should be render correctly', () => {
    const component = shallow(
      <Search
        search=""
        searchInputOnChange={() => {}}
        searchBy="title"
        searchByOnChange={() => {}}
        searchOnSubmit={() => {}}
      />,
    );
    expect(component).toMatchSnapshot();
  });

  it('should be call searchOnSubmit', () => {
    const searchOnSubmit = jest.fn();
    const component = mount(
      <Search
        search=""
        searchInputOnChange={() => {}}
        searchBy="title"
        searchByOnChange={() => {}}
        searchOnSubmit={searchOnSubmit}
      />,
    );
    component.find('button').simulate('click');
    expect(searchOnSubmit).toHaveBeenCalled();
  });

  it('should be call searchByOnChange', () => {
    const searchByOnChange = jest.fn();
    const component = mount(
      <Search
        search=""
        searchInputOnChange={() => {}}
        searchBy="title"
        searchByOnChange={searchByOnChange}
        searchOnSubmit={() => {}}
      />,
    );
    component.find('#searchTypeTitle').simulate('change');
    expect(searchByOnChange).toHaveBeenCalled();
  });

  it('should be call searchInputOnChange', () => {
    const searchInputOnChange = jest.fn();
    const component = mount(
      <Search
        search=""
        searchInputOnChange={searchInputOnChange}
        searchBy="title"
        searchByOnChange={() => {}}
        searchOnSubmit={() => {}}
      />,
    );
    component.find('input[type="text"]').simulate('change');
    expect(searchInputOnChange).toHaveBeenCalled();
  });
});
