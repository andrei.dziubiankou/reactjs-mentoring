// @flow
import React from 'react';
import injectSheet from 'react-jss';
import Header from '../header';

const styles = {
  head: {
    backgroundColor: 'grey',
    padding: '20px 20px',
  },
  search: {
    width: '700px',
  },
  searchControls: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  searchInput: {
    width: '100%',
    marginBottom: '15px',
  },
};

type Props = {
  searchBy: string,
  searchInputOnChange: Function,
  searchOnSubmit: Function,
  searchByOnChange: Function,
  search: string,
  classes: {
    head: {},
    search: {},
    searchControls: {},
    searchInput: {}
  }
};

const Search = ({
  classes, searchBy, searchInputOnChange, searchOnSubmit, searchByOnChange, search,
}: Props) => (
  <div className={classes.head}>
    <Header />
    <div className={classes.search}>
      <h3>Find your movie</h3>
      <input
        type="text"
        placeholder="type what you are looking for"
        className={classes.searchInput}
        onChange={searchInputOnChange}
        value={search}
      />
      <div className={classes.searchControls}>
        <div>
          Search by
          <label htmlFor="searchTypeTitle">
            <input
              type="radio"
              name="searchType"
              value="title"
              id="searchTypeTitle"
              onChange={searchByOnChange}
              checked={searchBy === 'title'}
            />
            Title
          </label>
          <label htmlFor="searchTypeGenre">
            <input
              type="radio"
              name="searchType"
              value="genres"
              id="searchTypeGenre"
              onChange={searchByOnChange}
              checked={searchBy === 'genres'}
            />
            Genre
          </label>
        </div>
        <button type="button" onClick={searchOnSubmit}>Search</button>
      </div>
    </div>
  </div>
);

export default injectSheet(styles)(Search);
