// @flow
import React from 'react';
import { withRouter } from 'react-router-dom';
import injectSheet from 'react-jss';
import { Film } from '../../prop-types/film-shape';

const styles = {
  filmPreview: {
    width: '30%',
    padding: '15px',
    margin: '15px 2.5%',
    border: '1px solid black',
    borderRadius: '5px',
    boxSizing: 'border-box',
    '&:first-child, &:nth-child(3n+1)': {
      marginLeft: 0,
    },
    '&:nth-child(3n), &:last-child': {
      marginRight: 0,
    },
  },
  filmPreviewImg: {
    width: '100%',
  },
};

type Props = {
  film: Film,
  history: {
    push: Function,
  },
  classes: {
    filmPreview: {},
    filmPreviewImg: {}
  }
};

const FilmPreview = ({ classes, film, history }: Props) => (
  <button
    type="button"
    className={classes.filmPreview}
    onClick={() => history.push(`/film/${film.id}`)}
  >
    <img src={film.poster_path} className={classes.filmPreviewImg} alt="" />
    <div>{film.release_date}</div>
    <div>{film.title}</div>
    <div>{film.genres ? film.genres.join(', ') : ''}</div>
  </button>
);

export default withRouter(injectSheet(styles)(FilmPreview));
