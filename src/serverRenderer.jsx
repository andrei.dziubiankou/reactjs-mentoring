import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { ChunkExtractor } from '@loadable/server';
import { fetchFilm, fetchFilms, searchChange } from './actions';
import rootReducer from './reducers';
import App from './app';

const path = require('path');

function renderHTML(html, preloadedState) {
  return `
      <!doctype html>
      <html>
        <head>
          <meta charset=utf-8>
          <title>React Server Side Rendering</title>
          <style>
            html,
            body {
              height: 100%;
              margin: 0;
            }

            * {
              box-sizing: border-box;
            }

            .app {
              width: 1200px;
              margin: auto;
              min-height: 100%;
              padding-bottom: 30px;
              position: relative;
            }
          </style>
          ${process.env.NODE_ENV === 'development' ? '' : '<link href="/main.css" rel="stylesheet" type="text/css">'}
        </head>
        <body>
          <div id="app" class="app">${html}</div>
          <script>
            // WARNING: See the following for security issues around embedding JSON in HTML:
            // http://redux.js.org/docs/recipes/ServerRendering.html#security-considerations
            window.PRELOADED_STATE = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
          </script>
          <script src="/client.bundle.js"></script>
        </body>
      </html>
  `;
}

export default function serverRenderer() {
  return (req, res) => {
    const webStats = path.resolve('./dist/client.json');
    const webExtractor = new ChunkExtractor({ statsFile: webStats });

    const store = createStore(
      rootReducer,
      applyMiddleware(thunkMiddleware),
    );
    // This context object contains the results of the render
    const context = {};
    const jsx = webExtractor.collectChunks(<App
      context={context}
      location={req.url}
      Router={StaticRouter}
      store={store}
    />);

    renderToString(jsx);

    // context.url will contain the URL to redirect to if a <Redirect> was used
    if (context.url) {
      res.writeHead(302, {
        Location: context.url,
      });
      res.end();
      return;
    }
    let preloadedState = {};

    if (req.url.includes('search')) {
      const search = decodeURI(req.url).split('search/')[1];
      const { searchParams } = store.getState();
      store.dispatch(searchChange(search));
      store.dispatch(fetchFilms({ ...searchParams, search }))
        .then(() => {
          preloadedState = store.getState();
          const htmlString = renderToString(jsx);

          res.send(renderHTML(htmlString, preloadedState));
        });
    } else if (req.url.includes('film')) {
      const filmId = decodeURI(req.url).split('film/')[1];
      store.dispatch(fetchFilm(filmId))
        .then(() => {
          preloadedState = store.getState();
          const htmlString = renderToString(jsx);

          res.send(renderHTML(htmlString, preloadedState));
        });
    } else {
      const htmlString = renderToString(jsx);

      res.send(renderHTML(htmlString, preloadedState));
    }
  };
}
