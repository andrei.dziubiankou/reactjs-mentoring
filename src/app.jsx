import 'isomorphic-fetch';
import 'babel-polyfill';
import loadable from '@loadable/component';
import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import Footer from './components/footer';
import ErrorBoundary from './components/error-boundary';

const HomePage = loadable(() => import('./components/home-page'));
const FilmPage = loadable(() => import('./components/film-page'));
const NoMatch = loadable(() => import('./components/no-match'));

const App = ({
  Router, location, context, store,
}) => (
  <Provider store={store}>
    <ErrorBoundary>
      <Router location={location} context={context}>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/home" component={HomePage} />
          <Route path="/film/:id" component={FilmPage} />
          <Route path="/search/:search?" component={HomePage} />
          <Route component={NoMatch} />
        </Switch>
      </Router>
      <Footer />
    </ErrorBoundary>
  </Provider>
);

App.defaultProps = {
  location: null,
  context: null,
};

App.propTypes = {
  Router: PropTypes.func.isRequired,
  location: PropTypes.string,
  context: PropTypes.shape({
    url: PropTypes.string,
  }),
  store: PropTypes.shape({
    dispatch: PropTypes.func.isRequired,
    getState: PropTypes.func.isRequired,
  }).isRequired,
};

export default App;
