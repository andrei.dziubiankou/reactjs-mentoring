/* eslint-env browser */
import { loadableReady } from '@loadable/component';
import React from 'react';
import { hydrate } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers';
import App from './app';

const store = createStore(
  rootReducer,
  window.PRELOADED_STATE,
  applyMiddleware(thunkMiddleware),
);

loadableReady(() => {
  const appRoot = document.getElementById('app');
  hydrate(<App Router={BrowserRouter} store={store} />, appRoot);
});
