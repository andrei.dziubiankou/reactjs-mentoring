import { combineReducers } from 'redux';
import { List } from 'immutable';

export function films(state = List([]), action) {
  switch (action.type) {
    case 'LOAD_FILMS':
      return List(action.films);
    case 'SET_DEFAULT':
      return List([]);
    default:
      return state;
  }
}

export function selectedFilm(state = null, action) {
  switch (action.type) {
    case 'LOAD_FILM':
      return action.film;
    default:
      return state;
  }
}

export function searchParams(state = {
  sortBy: 'vote_average',
  searchBy: 'title',
  search: '',
  sortOrder: 'desc',
}, action) {
  switch (action.type) {
    case 'CHANGE_SEARCH':
      return { ...state, search: action.search };
    case 'CHANGE_SORTBY':
      return { ...state, sortBy: action.sortBy };
    case 'CHANGE_SEARCHBY':
      return { ...state, searchBy: action.searchBy };
    case 'SET_DEFAULT':
      return {
        sortBy: 'vote_average',
        searchBy: 'title',
        search: '',
        sortOrder: 'desc',
      };
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  films,
  selectedFilm,
  searchParams,
});

export default rootReducer;
