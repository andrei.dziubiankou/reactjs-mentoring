import { List } from 'immutable';
import { films, selectedFilm, searchParams } from './index';

describe('Reducers', () => {
  it('should be return correct films', () => {
    const action = {
      type: 'LOAD_FILMS',
      films: List([1, 2, 3]),
    };
    const state = films(undefined, action);
    expect(state).toEqual(action.films);
  });

  it('should be return correct selectedFilm', () => {
    const action = {
      type: 'LOAD_FILM',
      film: { title: 'film example' },
    };
    const state = selectedFilm(undefined, action);
    expect(state).toEqual(action.film);
  });

  it('should be return correct searchPArams', () => {
    const testValue = 'test value';
    const actionChangeSearch = {
      type: 'CHANGE_SEARCH',
      search: testValue,
    };
    const actionChangeSortBy = {
      type: 'CHANGE_SORTBY',
      sortBy: testValue,
    };
    const actionChangeSearchBy = {
      type: 'CHANGE_SEARCHBY',
      searchBy: testValue,
    };
    const initParams = {
      sortBy: 'vote_average',
      searchBy: 'title',
      search: '',
      sortOrder: 'desc',
    };
    let state = searchParams(initParams, actionChangeSearch);
    expect(state.search).toEqual(testValue);
    state = searchParams(initParams, actionChangeSortBy);
    expect(state.sortBy).toEqual(testValue);
    state = searchParams(initParams, actionChangeSearchBy);
    expect(state.searchBy).toEqual(testValue);
  });
});
