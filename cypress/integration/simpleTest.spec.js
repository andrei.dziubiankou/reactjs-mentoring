describe('App initialization', () => {
  it('Visit localhost:9000 and find app container', () => {
    cy.visit('http://localhost:9000/');
    cy.get('#homepage .film-preview').should('have.length', 8).and('contain','horror');
    cy.get('#app .search__input').type('horror1');
    cy.get('#searchTypeGenre').click();
    cy.get('#app .search__controls button').click();
    cy.get('#homepage .film-preview').should('have.length', 4).and('contain', 'horror1');
  })
})
