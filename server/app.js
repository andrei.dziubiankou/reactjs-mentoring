const express = require('express');
const app = express();
const serverRenderer = require('../dist/server.bundle.js').default;

app.use(express.static('dist'));
app.use(serverRenderer());

module.exports = app;
