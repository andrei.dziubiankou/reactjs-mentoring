const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const LoadablePlugin = require('@loadable/webpack-plugin');

const config = {
  entry: './src/index.jsx',
  output: {
    path: path.resolve('dist'),
    filename: 'client.bundle.js',
    publicPath: '/',
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  devtool: 'source-map',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000,
    historyApiFallback: true,
    publicPath: ''
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader'
      },
      {
        test: /\.(css|scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'main.css',
      chunkFilename: '[id].css'
    }),
    new LoadablePlugin({ filename: 'client.json'}),
    new HtmlWebpackPlugin({template: './src/index.html'})
  ]
};

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    console.log('you use development mode');
  }

  if (argv.mode === 'production') {
    console.log('you use production mode');
    config.devtool = '';
  }

  return config;
};
