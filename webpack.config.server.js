const path = require('path');
const nodeExternals = require('webpack-node-externals');

const config = {
  target: 'node',
  entry: './src/serverRenderer.jsx',
  externals: [nodeExternals()],
  output: {
    path: path.resolve('dist'),
    filename: 'server.bundle.js',
    libraryTarget: 'commonjs2',
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader'
      },
      {
        test: /\.(css|scss)$/,
        use: [
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
};

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    console.log('you use development mode');
  }

  if (argv.mode === 'production') {
    console.log('you use production mode');
    config.devtool = '';
  }

  return config;
};
