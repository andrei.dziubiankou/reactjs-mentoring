import React from 'react';
import { MemoryRouter as Router } from 'react-router-dom';
import { storiesOf, addDecorator } from '@storybook/react';
import FilmFullInfo from '../src/components/film-full-info';
import FilmPreview from '../src/components/film-preview';
import FilmsList from '../src/components/films-list';
import Search from '../src/components/search';
import films from '../src/data/films';

const film = {
  poster_path: 'https://pp.userapi.com/c851036/v851036500/c9fad/DuCvMaUfXKw.jpg',
  title: 'Name3',
  release_date: '1991',
  genres: ['horror'],
  overview: 'description',
  vote_average: 2,
  runtime: 153,
};

storiesOf('Film Full Info', module)
  .addDecorator(story => (
    <Router initialEntries={['/foo']}>{story()}</Router>
  ))
  .add('default', () => (
    <FilmFullInfo film={film}/>
  ));

storiesOf('Film preview', module)
  .addDecorator(story => (
    <Router initialEntries={['/foo']}>{story()}</Router>
  ))
  .add('default', () => (
    <FilmPreview film={film}/>
  ));

storiesOf('Films List', module)
  .addDecorator(story => (
    <Router initialEntries={['/foo']}>{story()}</Router>
  ))
  .add('default', () => (
    <FilmsList films={films}/>
  ));

storiesOf('Search', module)
  .addDecorator(story => (
    <Router initialEntries={['/foo']}>{story()}</Router>
  ))
  .add('default', () => (
    <Search search=""
      searchInputOnChange={() => {}}
      searchBy="title"
      searchByOnChange={() => {}}
      searchOnSubmit={() => {}}
    />
  ));
